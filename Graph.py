from Node import Node


# Graph class that tracks which nodes are connected and disconnected
class Graph:
    def __init__(self):
        self.nodes = {}         # Map Node name (str) -> Node
        self.connected = {}     # Map Node name (str) -> set(Node)
        self.disconnected = {}  # Map Node name (str) -> set(Node)
        self.edge_count = 0

    # Add a node to the graph with modified edges
    def add_node(self, name: str):
        self.nodes[name] = Node(name)

    # track an edge between two nodes
    def add_edge(self, n1s: str, n2s: str):
        self.edge_count = self.edge_count + 1
        n1 = self._get_node(n1s)
        n2 = self._get_node(n2s)
        n1.add_edge(n2)
        n2.add_edge(n1)
        self._track_connect(n1, n2)
        # self._track_disconnect(n1, n2)
        # self._track_disconnect(n2, n1)

    def get_nodes(self) -> set:
        return set(self.nodes.values())

    # check if two nodes are connected
    def are_connected(self, n1: Node, n2: Node) -> bool:
        return n1.has_edge(n2)

    def count_nodes(self) -> int:
        return len(self.nodes)

    def count_edges(self) -> int:
        return self.edge_count

    def count_antiedges(self):
        n = self.count_nodes()
        return (n * ((n - 1) / 2)) - self.count_edges()

    # Keep track of the set of all nodes connected to n1 and n2
    def _track_connect(self, n1: Node, n2: Node):
        self.connected[n1.name] = n1.get_neighbors()
        self.connected[n2.name] = n2.get_neighbors()

    # Keep track of the set of all nodes NOT connected to n1, accounting for neighbor other
    # Very expensive method!
    def _track_disconnect(self, node: Node, other: Node):
        if node not in self.disconnected:
            self.disconnected[node.name] = set(self.nodes.values()) - {node} - node.get_neighbors()
        else:
            disconnected = set(self.disconnected[node.name])
            disconnected.remove(other)
            self.disconnected[node.name] = disconnected

    def _get_node(self, ns: str) -> Node:
        if ns not in self.nodes:
            node = Node(ns)
            self.nodes[ns] = node
            return node

        return self.nodes.get(ns)

    def _print_disconnected(self):
        for (k, v) in self.disconnected.items():
            connected = [str(n) for n in v]
            connected.sort()
            print("%s is not connected to: %s" % (k, ", ".join(connected)))

    def _print_connected(self):
        for (k, v) in self.connected.items():
            connected = [str(n) for n in v]
            connected.sort()
            print("%s is connected to: %s" % (k, ", ".join(connected)))
