import sys

if len(sys.argv) != 3:
    raise Exception("Input: <pentagon | saladfingers | crown | circle> <node count | level depth>")

shape = sys.argv[1]
count = int(sys.argv[2])

if count <= 0:
    raise ValueError("Count/depth must be at least 1")

is_pentagon = shape == "pentagon"
is_saladfingers = shape == "saladfingers"
is_circle = shape == "circle"
is_crown = shape == "crown"
edges = []

if is_pentagon or is_saladfingers:
    levels = count
    if is_pentagon:
        node_num = 0
        for l in range(levels):
            if l > 0:
                c1n1 = node_num - 1
                c1n2 = c1n1 + 5
                c2n1 = c1n1 - 3
                c2n2 = c2n1 + 5
                edges.append(("N%d" % c1n1, "N%d" % c1n2))
                edges.append(("N%d" % c2n1, "N%d" % c2n2))
            for i in range(5):
                n1 = "N%d" % node_num
                next_num = node_num + 1
                if i == 4:
                    next_num = next_num - 5
                n2 = "N%d" % next_num
                edges.append((n1, n2))
                node_num = node_num + 1
    elif is_saladfingers:
        pass
elif is_circle or is_crown:
    if is_circle:
        pass
    elif is_crown:
        if count % 2 == 1:
            count = count + 1
        half = int(count / 2)
        for x in range(1, half + 1):
            pass  # WIP
else:
    raise ValueError("Input unknown shape %s" % shape)

with open("./test/%s.test" % shape, "w") as file:
    file.write(shape)
    if is_pentagon or is_saladfingers:
        file.write(" %d" % count)
    file.write("\n")
    edges_formatted = ["%s %s" % e for e in edges]
    content = "\n".join(edges_formatted).strip()
    file.write(content)
