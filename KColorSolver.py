from Node import Node
from Graph import Graph
from random import shuffle


class KColorSolver:
    def __init__(self, g: Graph):
        self.g = g
        self.color_groups = []
        # for x in range(0, len(self.colors)):
        # self.color_groups.append(set())
        self._arrange_groups()
        self.max_color = 0

    def _arrange_groups(self):
        # self.groups = {0: {'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8'}}  # for testing
        self.color_groups.append(self.g.get_nodes())

    def solve(self):
        # self._solve(0)
        self._find_node_colors()
        self.max_color = 0
        self._find_node_colors()
        self.color_groups = [g for g in self.color_groups if len(g) > 0]

    def color_count(self):
        return self.max_color + 1 #len(self.color_groups)

    def print_stats(self):
        print("Finished with %d colors, %d nodes, %d edges, %d anti-edges." %
              (self.color_count(), self.g.count_nodes(), self.g.count_edges(), self.g.count_antiedges()))

    # Method returns key for sorting nodes
    def _group_key(self, n: Node):
        return n.degree()

    # Compute the fixed point for every level
    def _solve(self, level):
        if self.color_count() <= level:
            return
        self._fixed_point(level)
        self._solve(level + 1)

    # For the current level, remove enough nodes such that no two neighbors are in the same color group.
    # All removed neighbors pushed to the next color group.
    def _fixed_point(self, level):
        orig_group = set(self.color_groups[level])  # Avoid concurrent modification issues
        if len(orig_group) <= 1:
            return
        # shuffle(orig_group)  # For more solution uniqueness, not necessary
        next_level = level + 1
        for head in orig_group:
            group = self.color_groups[level]
            if head in group:
                neighbors = head.get_neighbors()
                connected = group.intersection(neighbors)
                if len(connected) != 0:
                    if next_level >= self.color_count():
                        self.color_groups.append(set())
                    connected = self._attempt_flip(connected)
                    self.color_groups[level] = group.difference(connected)
                    self.color_groups[next_level] = self.color_groups[next_level] | connected

    def _attempt_flip(self, nodes):
        for n in nodes:
            for neighbor in n.get_neighbors():
                pass

    def _find_node_colors(self):
        # TODO fails on pentagon level 11
        nodes = sorted(self.g.get_nodes(), reverse=True, key=self._group_key)
        for n in nodes:
            n.pick_color()
            self.max_color = max(self.max_color, n.color)

    def _dfs_lazy_flip(self):
        pass
        #V8
        #V1 -> !V6 = true
        #    V6 -> !V3 = false
        #        V3 -> !V5 = true
        #            V5 -> !V2 = false
        #                V2 -> !V7 = true
        #                    V7 -> !V4 = false
        #                        V4 -> true
        #                V4 ----
        #        V4 ----
        #    V7 ---- maybe track with change signatures?
        #V2 ----
        #V3 ----