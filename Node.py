class Node:
    def __init__(self, name):
        self.name = name
        self.neighbors = {}     # Map Node name (str) -> Node
        self.color = 0

    def add_edge(self, other: 'Node'):
        self.neighbors[other.name] = other

    def has_edge(self, other) -> bool:
        return other.name in self.neighbors if isinstance(other, Node) else other in self.neighbors

    def get_neighbors(self) -> set:
        return set(self.neighbors.values())

    def degree(self) -> int:
        return len(self.neighbors.values())

    def get_neighbor_colors(self) -> set:
        return set([n.color for n in self.get_neighbors()])

    def pick_color(self):
        neighbor_colors = self.get_neighbor_colors()
        #if self.color in neighbor_colors:
        max_color = max(neighbor_colors)
        next_color = max_color + 1
        for i in range(next_color):
            if i not in neighbor_colors:
                self.color = i
                return
        self.color = next_color


    def __str__(self):
        return self.name