from KColorSolver import KColorSolver
from Node import Node
from math import cos, sin, sqrt, pi
import re


class TikzPrinter:
    def __init__(self, shape, solver: KColorSolver, test_name):
        self.test_name = test_name
        shape_num = shape.split()
        self.shape = shape_num[0]
        if len(shape_num) > 1:
            self.count = int(shape_num[1])
            self.levels = self.count
        self.solver = solver
        self.colors = ['#2ecc71', '#E32636', '#9966CC', '#3498db', '#f1c40f', '#C0C0C0',
                       '#D4AF37', '#FF69B4', '#FF4F00', '#B5651D', '#002FA7', '#48D1CC',
                       '#3EB489', '#3B444B', '#39FF14', '#673147', '#682860', '#FF355E',
                       '#00CCCC', '#004040', '#522D80', '#674846', '#4169E1', '#E18E96',
                       '#FF8C69', '#C2B280', '#92000A', '#76FF7A', '#FD0E35', '#7B1113',
                       '#FEFE33', '#FFFF00', '#738678', '#FC6C85', '#F5DEB3', '#F3E5AB']

    def print_tikz(self):
        file_name = "./out/%s.out" % self.test_name
        self.out_file = open(file_name, "w")
        self._print_colors()
        self._print_nodes()
        self._print_edges()
        self.out_file.close()
        print("Results in %s" % file_name)
        self.out_file = None

    def get_coordinates(self):
        if self.shape == "pentagon":
            coords = self._get_coordinates_pentagon()
        elif self.shape == "bars":
            coords = self._get_coordinates_bars()
        elif self.shape == "crown":
            coords = self._get_coordinates_crown()
        elif self.shape == "saladfingers":
            coords = self._get_coordinates_saladfingers()
        else:
            coords = self._get_coordinates_circle()
        nodes = [str(n) for n in self.solver.g.get_nodes()]
        nodes = sorted(nodes, key=self._alphanum_key)
        positions = dict(zip(nodes, coords))
        return positions

    def _try_int(self, s):
        try:
            return int(s)
        except ValueError:
            return s

    def _alphanum_key(self, s):
        return [self._try_int(c) for c in re.split('([0-9]+)', s)]

    def _print_nodes(self):
        positions = self.get_coordinates()
        discovered = set()
        for n in self.solver.g.get_nodes():    # (k, v) in enumerate(self.solver.color_groups):
            color = "group%d" % n.color
            #for n in v:
            #    if n in discovered:
            #        raise Exception("Node was in two groups: %s" % str(n))
            #    discovered.add(n)
            x, y = positions[n.name]
            print("\\node[draw, circle, fill=%s] (%s) at (%.2f, %.2f) {%s};" %
                  (color, str(n), x, y, str(n)), file=self.out_file)

    def _print_edges(self):
        discovered = set()
        for (k, v) in self.solver.g.connected.items():
            discovered.add(k)
            for neighbor in v:
                if neighbor.name not in discovered:
                    print("\\path (%s) edge (%s)" % (k, str(neighbor)), file=self.out_file)

    def _print_colors(self):
        for i in range(0, self.solver.color_count()):
            color = self.colors[i].replace("#", "")
            print("\\definecolor{group%d}{HTML}{%s}" % (i, color), file=self.out_file)

    def _get_coordinates_circle(self):
        positions = []
        num_nodes = self.solver.g.count_nodes()
        radius = sqrt(num_nodes)
        space = (pi * 2) / float(num_nodes)
        for i in range(0, num_nodes + 1):
            t = i * space
            x = radius * cos(t)
            y = radius * sin(t)
            positions.append((x, y))
        # shuffle(positions)
        return positions

    def _get_coordinates_bars(self):
        positions = []
        num_nodes = self.solver.g.count_nodes()
        raise NotImplementedError

    def _get_coordinates_pentagon(self):
        positions = []
        levels = self.levels if self.levels is not None else int(self.solver.g.count_nodes() / 5)
        inc = 1
        for l in range(1, levels + 1):
            radius = sqrt(5) + inc
            space = (pi * 2) / 5.0
            for i in range(5):
                t = (i * space) + (pi / 2)
                x = radius * cos(t)
                y = radius * sin(t)
                # print("(%.2f, %.2f)" % (x,y))
                positions.append((x, y))
            inc = inc + 2
        return positions

    def _get_coordinates_crown(self):
        positions = []
        num_nodes = self.solver.g.count_nodes()
        raise NotImplementedError

    def _get_coordinates_saladfingers(self):
        positions = []
        num_nodes = self.solver.g.count_nodes()
        raise NotImplementedError
