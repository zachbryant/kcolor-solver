from Graph import Graph
from KColorSolver import KColorSolver
from TikzPrinter import TikzPrinter
import sys


def main():
    g = Graph()
    edges = set()
    argc = len(sys.argv)
    if argc < 2:
        test_name = input("Enter a test file name:")
    else:
        test_name = sys.argv[1]
    with open(test_name) as test:
        shape = test.readline().strip().lower()
        line_num = 2
        # Populate nodes, prep edges (don't build)
        for line in test.readlines():
            n1, n2 = line.strip().split()
            if n1 != "" and n2 != "":
                g.add_node(n1)
                g.add_node(n2)
                edges.add((n1, n2))
            else:
                print("Malformed line (line %d)" % line_num)
            line_num = line_num + 1

    # Populate edges
    for (n1, n2) in edges:
        g.add_edge(n1, n2)

    solver = KColorSolver(g)
    solver.solve()
    solver.print_stats()
    printer = TikzPrinter(shape, solver, test_name.split("/")[-1].split(".")[0])
    printer.print_tikz()


if __name__ == "__main__":
    main()
